using System.Collections;
using System.Collections.Generic;
using Mirror;
using UnityEngine;

public class BeastNetworkManager : NetworkManager {

    enum ClientTypeOverride {
        Server,
        Host,
        Client
    }

    [SerializeField] ClientTypeOverride clientTypeOverride;
    [SerializeField] bool skipLogin = false;

    void OnEnable () {
        AccountManager.OnAccountSuccess.AddListener (AccountSuccess);
    }
    void OnDisable () {
        AccountManager.OnAccountSuccess.RemoveListener (AccountSuccess);
    }

    void AccountSuccess () {
#if UNITY_EDITOR
        if (clientTypeOverride == ClientTypeOverride.Host) {
            StartHost ();
        } else if (clientTypeOverride == ClientTypeOverride.Client) {
#endif

            StartClient ();

#if UNITY_EDITOR
        }
#endif
    }

    public override void Start () {
        base.Start ();

#if UNITY_EDITOR
        if (skipLogin) {
            AccountManager.playerProfileModel = new PlayFab.ClientModels.PlayerProfileModel () {
                DisplayName = "Dummy"
            };

            AccountSuccess ();
        }
        if (clientTypeOverride == ClientTypeOverride.Server) {
            StartServer ();
        }
#endif
    }

}