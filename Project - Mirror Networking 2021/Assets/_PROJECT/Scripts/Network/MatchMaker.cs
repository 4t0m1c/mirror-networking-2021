using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MatchMaker : Singleton<MatchMaker> {

    [Header ("Round Settings")]
    [SerializeField] int minPlayers = 2;
    [SerializeField] int maxPlayers = 4;
    [SerializeField] float roundCountdown = 5;

    [Header ("Debug")]
    [SerializeField] bool roundStarting = false;
    [SerializeField] bool roundInProgress = false;
    [SerializeField] List<Player> playerQueue = new List<Player> ();

    public void AddPlayer (Player player) {
        Debug.Log ($"Player added to queue {player.netId}");
        playerQueue.Add (player);

        if (playerQueue.Count >= minPlayers && !roundStarting && !roundInProgress) {
            roundStarting = true;
            StartCoroutine (ProcessQueue ());
        }
    }

    public void RemovePlayer (Player player) {
        Debug.Log ($"Player removed from queue {player.netId}");
        playerQueue.Remove (player);

        if (playerQueue.Count < minPlayers && roundStarting) {
            roundStarting = false;
        }
    }

    IEnumerator ProcessQueue () {
        float currentCountDown = roundCountdown;
        while (roundStarting) {

            if (currentCountDown > 0) {
                currentCountDown -= Time.deltaTime;
                Debug.Log ($"Time left: {currentCountDown}");
            } else {
                //Ready to start round
                List<Player> players = new List<Player> ();
                for (var i = 0; i < playerQueue.Count; i++) {
                    if (i < maxPlayers) {
                        players.Add (playerQueue[i]);
                    } else {
                        break;
                    }
                }
                //Remove from queue
                for (var i = 0; i < players.Count; i++) {
                    playerQueue.RemoveAt (0);
                }

                BeginRound (players);
                roundStarting = false;
            }

            yield return null;
        }
    }

    void BeginRound (List<Player> players) {
        roundInProgress = true;
        Debug.Log ($"Round Starting - [{players.Count}] players");

        if (BeastNetworkManager.singleton.mode == Mirror.NetworkManagerMode.ServerOnly) {
            StartCoroutine (LoadGameScene ());
        }

        foreach (Player player in players) {
            player.PlayGame ();
        }
    }

    IEnumerator LoadGameScene () {
        AsyncOperation loading = SceneManager.LoadSceneAsync (2, LoadSceneMode.Additive);

        while (!loading.isDone) yield return null;
    }

    public void EndRound () {
        roundInProgress = false;
    }

}

public static class MatchExtensions {
    public static System.Guid ToGuid (this string id) {
        System.Security.Cryptography.MD5CryptoServiceProvider provider = new System.Security.Cryptography.MD5CryptoServiceProvider ();
        byte[] inputBytes = System.Text.Encoding.Default.GetBytes (id);
        byte[] hashBytes = provider.ComputeHash (inputBytes);

        return new System.Guid (hashBytes);
    }
}