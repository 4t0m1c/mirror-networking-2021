using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIChatEntry : MonoBehaviour {

    [SerializeField] Text text;

    public void SetMessage (string message, PoolUIChatEntry poolUIChatEntry) {
        text.text = message;
        LayoutRebuilder.ForceRebuildLayoutImmediate ((RectTransform) transform.parent);

        StartCoroutine (FadeOut (10, poolUIChatEntry));
    }

    IEnumerator FadeOut (float time, PoolUIChatEntry poolUIChatEntry) {
        float timeLeft = time;
        while (timeLeft > 0) {
            timeLeft -= Time.deltaTime;
            yield return null;
        }
        poolUIChatEntry.ReturnToObjectPool (this);
    }

}