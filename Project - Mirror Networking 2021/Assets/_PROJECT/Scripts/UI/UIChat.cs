using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIChat : MonoBehaviour {
    [SerializeField] PoolUIChatEntry poolUIChatEntry;
    [SerializeField] InputField chatInputField;

    bool chatOpen = false;

    string message;

    void OnEnable () {
        InputManager.OnChatPerformed.AddListener (ChatPerformed);
        Player.OnChatMessageReceived.AddListener (ChatReceived);
    }

    void OnDisable () {
        InputManager.OnChatPerformed.RemoveListener (ChatPerformed);
        Player.OnChatMessageReceived.RemoveListener (ChatReceived);
    }

    void ChatPerformed () {
        Debug.Log ($"UIChat: ChatPerformed");
        chatOpen = !chatOpen;

        chatInputField.gameObject.SetActive (chatOpen);

        if (chatOpen) {
            chatInputField.Select ();
            chatInputField.ActivateInputField ();
        } else {
            if (message.Length > 0) {
                Debug.Log ($"Sending message: [{message}]");

                string _message = $"<color=white>{AccountManager.playerProfileModel.DisplayName}:</color> <i>{message}</i>";
                Player.localPlayer.SendChatMessage (_message);
                
                chatInputField.text = string.Empty;
            }
        }
    }

    void ChatReceived (string message) {
        UIChatEntry entry = poolUIChatEntry.GetFromObjectPool ();
        entry.SetMessage (message, poolUIChatEntry);
    }

    //Updated by the UI InputField
    public void UpdateMessage (string message) {
        Debug.Log ($"Message updated: {message}");
        this.message = message;
    }

}