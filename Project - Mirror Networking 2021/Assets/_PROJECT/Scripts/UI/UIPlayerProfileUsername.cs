using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent (typeof (Text))]
public class UIPlayerProfileUsername : MonoBehaviour {
    Text text;

    void Awake () {
        text = GetComponent<Text> ();
    }

    void OnEnable () {
        if (AccountManager.playerProfileModel == null) {
            AccountManager.OnPlayerProfileReceived.AddListener (UpdatePlayerProfile);
        } else {
            UpdatePlayerProfile ();
        }
    }
    void OnDisable () {
        AccountManager.OnPlayerProfileReceived.RemoveListener (UpdatePlayerProfile);
    }

    void UpdatePlayerProfile () {
        text.text = AccountManager.playerProfileModel.DisplayName.ToUpper();
    }
}