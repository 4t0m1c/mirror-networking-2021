using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIActionLogEntry : MonoBehaviour {

    [SerializeField] Text senderText, receiverText;
    [SerializeField] Image actionIcon;

    [System.Serializable]
    class ActionIcons {
        public PlayerActionTypes actionType;
        public Sprite icon;
    }

    [SerializeField] List<ActionIcons> actionIcons = new List<ActionIcons> ();

    public void SetActionLogEntry (ActionEvent actionEvent, PoolUIActionLogEntry poolUIActionLogEntry) {

        senderText.text = actionEvent.sender;
        receiverText.text = actionEvent.receiver;

        foreach (var _action in actionIcons) {
            if (_action.actionType == actionEvent.playerAction) {
                actionIcon.sprite = _action.icon;
            }
        }

        LayoutRebuilder.ForceRebuildLayoutImmediate ((RectTransform) transform.parent);

        StartCoroutine (FadeOut (10, poolUIActionLogEntry));
    }

    IEnumerator FadeOut (float time, PoolUIActionLogEntry poolUIActionLogEntry) {
        float timeLeft = time;
        while (timeLeft > 0) {
            timeLeft -= Time.deltaTime;
            yield return null;
        }
        poolUIActionLogEntry.ReturnToObjectPool (this);
    }

}