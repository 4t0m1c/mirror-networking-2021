using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIActionLog : MonoBehaviour {
    [SerializeField] PoolUIActionLogEntry poolUIActionLogEntry;

    void OnEnable () {
        Player.OnActionLogReceived.AddListener (ActionLogReceived);
    }

    void OnDisable () {
        Player.OnActionLogReceived.RemoveListener (ActionLogReceived);
    }

    void ActionLogReceived (ActionEvent actionEvent) {
        UIActionLogEntry actionLogEntry = poolUIActionLogEntry.GetFromObjectPool();
        actionLogEntry.SetActionLogEntry(actionEvent, poolUIActionLogEntry);
    }

}

public struct ActionEvent {
    public string sender;
    public string receiver;
    public PlayerActionTypes playerAction;

    public ActionEvent (string sender, string receiver, PlayerActionTypes playerAction) {
        this.sender = sender;
        this.receiver = receiver;
        this.playerAction = playerAction;
    }
}

public enum PlayerActionTypes {
    Kill
}