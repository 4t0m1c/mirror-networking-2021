using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class UILobby : MonoBehaviour {

    [SerializeField] Canvas canvas;
    [SerializeField] CanvasGroup canvasGroup;
    [SerializeField] GameObject lobbyCamera;
    [SerializeField] Text buttonText;

    bool queued = false;

    void OnEnable () {
        Player.OnGameSceneLoaded.AddListener (OnGameSceneLoaded);
        Player.OnGameSceneUnloaded.AddListener (OnGameSceneUnloaded);
    }

    void OnDisable () {
        Player.OnGameSceneLoaded.RemoveListener (OnGameSceneLoaded);
        Player.OnGameSceneUnloaded.RemoveListener (OnGameSceneUnloaded);
    }

    public void PlayGame () {
        if (!queued) {
            queued = true;
            Player.localPlayer.QueuePlayer ();
            buttonText.text = "STOP";
        } else {
            queued = false;
            Player.localPlayer.DequeuePlayer ();
            buttonText.text = "PLAY";
        }
    }

    void OnGameSceneLoaded () {
        canvas.enabled = false;
        canvasGroup.interactable = false;
        lobbyCamera.SetActive (false);

        buttonText.text = "PLAY";
        queued = false;
    }

    void OnGameSceneUnloaded () {
        canvas.enabled = true;
        canvasGroup.interactable = true;
        lobbyCamera.SetActive (true);
    }

}