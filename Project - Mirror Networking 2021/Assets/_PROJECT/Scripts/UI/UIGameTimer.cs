using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIGameTimer : MonoBehaviour {

    Text text;

    void Awake () {
        text = GetComponent<Text> ();
    }

    void OnEnable () {
        Player.OnGameTimeUpdated.AddListener (GameTimeUpdated);
    }

    void OnDisable() {
        Player.OnGameTimeUpdated.RemoveListener (GameTimeUpdated);
    }

    void GameTimeUpdated (float time) {
        text.text = time.ToString("n0");
    }

}