using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIScoreboard : MonoBehaviour {
    [SerializeField] PoolUIScoreboardEntry poolUIScoreboardEntry;

    [SerializeField] Canvas canvas;

    void OnEnable () {
        InputManager.OnScoreboardStarted.AddListener (ScoreboardStarted);
        InputManager.OnScoreboardCanceled.AddListener (ScoreboardCanceled);
    }

    void OnDisable () {
        InputManager.OnScoreboardStarted.RemoveListener (ScoreboardStarted);
        InputManager.OnScoreboardCanceled.RemoveListener (ScoreboardCanceled);
    }

    void ScoreboardStarted () {
        Debug.Log ($"UIScoreboard: ScoreboardStarted");
        canvas.enabled = true;
    }

    void ScoreboardCanceled () {
        Debug.Log ($"UIScoreboard: ScoreboardCanceled");
        canvas.enabled = false;
    }
}