using System;
using System.Collections;
using System.Collections.Generic;
using Mirror;
using UnityEngine;

namespace At0m1c {
    public class PlayerController : NetworkBehaviour {

        //Controller
        [SerializeField] new Rigidbody rigidbody;
        [SerializeField] float moveSpeed = 50;
        Vector2 moveVector;

        //Interactables
        List<NetworkIdentity> interactables = new List<NetworkIdentity> ();

        bool isHolding = false;
        NetworkIdentity holdingItem;

        new Camera camera;

        void Start () { }

        public override void OnStartServer () {
            Debug.Log ($"OnStartServer");
        }

        public override void OnStartClient () {
            Debug.Log ($"OnStartClient");
        }

        public override void OnStartLocalPlayer () {
            Debug.Log ($"OnStartLocalPlayer");

            // Player.OnGameSceneLoaded.AddListener (InputsRegistration);
        }

        void OnEnable () {
            if (isLocalPlayer) {
                if (camera == null) {
                    camera = Camera.main;
                    camera.tag = "Untagged";
                } else {
                    if (Camera.main != null) {
                        Destroy (Camera.main.gameObject);
                    }
                }
                camera.gameObject.SetActive (true);

                InputsRegistration ();
            }
        }

        void OnDisable () {
            if (isLocalPlayer) {
                InputManager.OnMovementUpdated.RemoveListener (MovementUpdated);
                InputManager.OnInteractPerformed.RemoveListener (InteractPerformed);

                InputManager.OnChatPerformed.RemoveListener (ChatPerformed);
                InputManager.OnCancelPerformed.RemoveListener (CancelPerformed);

                InputManager.OnScoreboardStarted.RemoveListener (ScoreboardStarted);
                InputManager.OnScoreboardCanceled.RemoveListener (ScoreboardCanceled);

                rigidbody.velocity = Vector3.zero;
                moveVector = Vector2.zero;
                camera.gameObject.SetActive (false);
            }
        }

        [Client]
        void InputsRegistration () {
            InputManager.OnMovementUpdated.AddListener (MovementUpdated);
            InputManager.OnInteractPerformed.AddListener (InteractPerformed);

            InputManager.OnChatPerformed.AddListener (ChatPerformed);
            InputManager.OnCancelPerformed.AddListener (CancelPerformed);

            InputManager.OnScoreboardStarted.AddListener (ScoreboardStarted);
            InputManager.OnScoreboardCanceled.AddListener (ScoreboardCanceled);

            StartCoroutine (ControllerUpdate ());

            rigidbody.isKinematic = false;
            camera.transform.SetParent (transform);
            camera.transform.localPosition = new Vector3 (0, camera.transform.localPosition.y, camera.transform.localPosition.z);
            camera.transform.localEulerAngles = new Vector3 (camera.transform.localEulerAngles.x, 0, 0);
        }

        void InteractPerformed () {
            Debug.Log ($"Interacting");
            //Look for closest interactable and collect
            if (interactables.Count > 0 && !isHolding) {
                foreach (var item in interactables) {
                    if (!item.hasAuthority) {
                        Player.localPlayer.PickupItem (item);
                        holdingItem = item;
                        isHolding = true;
                        break;
                    } else {
                        continue;
                    }
                }
            } else if (isHolding) {
                isHolding = false;
                Player.localPlayer.DropItem (holdingItem);
            }
        }

        void ChatPerformed () {
            Debug.Log ($"ChatPerformed");
        }

        void CancelPerformed () {
            Debug.Log ($"CancelPerformed");
        }

        void ScoreboardStarted () {
            Debug.Log ($"ScoreboardStarted");
        }

        void ScoreboardCanceled () {
            Debug.Log ($"ScoreboardCanceled");
        }

        #region Controller
        /*
            CONTROLLER
        */

        void MovementUpdated (Vector2 moveVector) {
            this.moveVector = moveVector;
        }

        IEnumerator ControllerUpdate () {
            Vector2 _moveVector = Vector2.zero;
            while (true) {
                _moveVector = moveVector * Time.deltaTime * moveSpeed;
                rigidbody.velocity = new Vector3 (_moveVector.x, rigidbody.velocity.y, _moveVector.y);
                yield return new WaitForFixedUpdate ();
            }
        }

        #endregion

        void OnTriggerEnter (Collider other) {
            IInteractable interactable = other.GetComponentInChildren<IInteractable> ();
            if (interactable != null) {
                interactables.Add (other.GetComponent<NetworkIdentity> ());
            }
        }

        void OnTriggerExit (Collider other) {
            IInteractable interactable = other.GetComponentInChildren<IInteractable> ();
            if (interactable != null) {
                interactables.Remove (other.GetComponent<NetworkIdentity> ());
            }
        }

    }
}