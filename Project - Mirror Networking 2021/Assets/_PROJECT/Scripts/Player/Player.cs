using System.Collections;
using System.Collections.Generic;
using At0m1c;
using Mirror;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

public class Player : NetworkBehaviour {

    public static Player localPlayer;
    public static UnityEvent OnGameSceneLoaded = new UnityEvent ();
    public static UnityEvent OnGameSceneUnloaded = new UnityEvent ();
    public static UnityEvent<float> OnGameTimeUpdated = new UnityEvent<float> ();
    public static UnityEvent<string> OnChatMessageReceived = new UnityEvent<string> ();
    public static UnityEvent<ActionEvent> OnActionLogReceived = new UnityEvent<ActionEvent> ();

    public UnityEvent OnGameLoaded = new UnityEvent ();
    public UnityEvent OnGameUnloaded = new UnityEvent ();

    bool isHolding = false;

    NetworkMatchChecker networkMatchChecker;
    PlayerController playerController;

    public override void OnStartServer () {
        Debug.Log ($"OnStartServer");
        networkMatchChecker = GetComponent<NetworkMatchChecker> ();
    }

    public override void OnStopServer () {
        Debug.Log ($"OnStopServer");
    }

    public override void OnStartClient () {
        Debug.Log ($"OnStartClient");
        playerController = GetComponent<PlayerController> ();
        playerController.enabled = false;

        if (!isLocalPlayer) {
            Debug.Log ($"Activate mesh");
        }
    }

    public override void OnStartLocalPlayer () {
        Debug.Log ($"OnStartLocalPlayer");
        localPlayer = this;
    }

    /*
        MATCHMAKING
    */

    [Client]
    public void QueuePlayer () {
        Debug.Log ($"QueuePlayer");
        CmdQueuePlayer ();
    }

    [Command]
    void CmdQueuePlayer () {
        MatchMaker.Instance.AddPlayer (this);
    }

    [Client]
    public void DequeuePlayer () {
        Debug.Log ($"DequeuePlayer");
        CmdDequeuePlayer ();
    }

    [Command]
    void CmdDequeuePlayer () {
        MatchMaker.Instance.RemovePlayer (this);
    }

    /*
        PLAY GAME
    */

    [Server]
    public void PlayGame () {
        Debug.Log ($"Play Game {netId}");
        networkMatchChecker.matchId = "playing".ToGuid ();
        GameManager.Instance.RegisterPlayer (this);
        TargetPlayGame ();
    }

    [TargetRpc]
    void TargetPlayGame () {
        Debug.Log ($"TargetPlayGame {netId}");
        StartCoroutine (LoadGameScene ());
    }

    IEnumerator LoadGameScene () {
        AsyncOperation loading = SceneManager.LoadSceneAsync (2, LoadSceneMode.Additive);

        while (!loading.isDone) yield return null;

        OnGameLoaded.Invoke ();
        OnGameSceneLoaded.Invoke ();
        CmdPlayGame ();
    }

    [Command]
    void CmdPlayGame () {
        GameManager.Instance.StartGame ();
    }

    IEnumerator UnloadGameScene () {
        AsyncOperation loading = SceneManager.UnloadSceneAsync (2);

        while (!loading.isDone) yield return null;

        OnGameUnloaded.Invoke ();
        OnGameSceneUnloaded.Invoke ();
    }

    /* 
        GAME STATE
    */

    [Server]
    public void StateWarmup (Vector3 spawnPosition) {
        TargetStateWarmup (spawnPosition);
    }

    [TargetRpc]
    void TargetStateWarmup (Vector3 spawnPosition) {
        transform.position = spawnPosition;
        Debug.Log ($"State: Warmup");
    }

    [Server]
    public void StateGameplay () {
        TargetStateGameplay ();
    }

    [TargetRpc]
    void TargetStateGameplay () {
        playerController.enabled = true;
        Debug.Log ($"State: Gameplay");
    }

    [Server]
    public void StatePostGame () {
        TargetStatePostGame ();
    }

    [TargetRpc]
    void TargetStatePostGame () {
        playerController.enabled = false;
        Debug.Log ($"State: Post Game");
    }

    [Server]
    public void StateEndGame () {
        networkMatchChecker.matchId = string.Empty.ToGuid ();
        TargetStateEndGame ();
    }

    [TargetRpc]
    void TargetStateEndGame () {
        Debug.Log ($"State: End Game");
        StartCoroutine (UnloadGameScene ());
    }

    /* 
        GAME TIME
    */

    [Server]
    public void GameTimeUpdated (float time) {
        TargetGameTimeUpdated (time);
    }

    [TargetRpc]
    void TargetGameTimeUpdated (float time) {
        // Debug.Log ($"Time updated: {time}");
        OnGameTimeUpdated.Invoke (time);
    }

    /*
        PICKUPS
    */

    [Client]
    public void PickupItem (NetworkIdentity interactable) {
        Debug.Log ($"Picking up item");
        CmdPickupItem (interactable);
    }

    [Command]
    void CmdPickupItem (NetworkIdentity identity) {
        identity.AssignClientAuthority (connectionToClient);
        RpcPickupItem (identity);
    }

    [ClientRpc]
    void RpcPickupItem (NetworkIdentity identity) {
        isHolding = true;
        StartCoroutine (ItemFollowPlayer (identity));
    }

    IEnumerator ItemFollowPlayer (NetworkIdentity identity) {
        while (isHolding && identity != null) {
            identity.transform.position = transform.position;
            identity.transform.Translate (Vector3.up + Vector3.forward);
            yield return null;
        }
        isHolding = false;
    }

    [Client]
    public void DropItem (NetworkIdentity interactable) {
        Debug.Log ($"Dropping item");
        CmdDropItem (interactable);
    }

    [Command]
    void CmdDropItem (NetworkIdentity identity) {
        if (identity != null) {
            identity.RemoveClientAuthority ();
            RpcDropItem ();
        }
    }

    [ClientRpc]
    void RpcDropItem () {
        isHolding = false;
    }

    /* CHAT */

    public void SendChatMessage (string message) {
        CmdSendChatMessage (message);

        //TEST
        ClientSendActionLog (new ActionEvent (AccountManager.playerProfileModel.DisplayName, string.Empty, PlayerActionTypes.Kill));
    }

    [Command]
    void CmdSendChatMessage (string message) {
        RpcSendChatMessage (message);

        //TEST
        ServerSendActionLog (new ActionEvent ("SERVER", string.Empty, PlayerActionTypes.Kill));
    }

    [ClientRpc]
    void RpcSendChatMessage (string message) {
        OnChatMessageReceived.Invoke (message);
    }

    /* ACTION LOG */

    [Client]
    public void ClientSendActionLog (ActionEvent actionEvent) {
        CmdSendActionLog (actionEvent);
    }

    [Server] 
    public void ServerSendActionLog (ActionEvent actionEvent) {
        RpcSendActionLog (actionEvent);
    }

    [Command]
    void CmdSendActionLog (ActionEvent actionEvent) {
        RpcSendActionLog (actionEvent);
    }

    [ClientRpc]
    void RpcSendActionLog (ActionEvent actionEvent) {
        OnActionLogReceived.Invoke (actionEvent);
    }

    /* SCOREBOARD */

    // [Client]
    // public void ClientSendScoreboardUpdated (ActionEvent actionEvent) {
    //     CmdSendScoreboardUpdated (actionEvent);
    // }

    // [Server] 
    // public void ServerSendScoreboardUpdated (ActionEvent actionEvent) {
    //     TargetSendScoreboardUpdated (actionEvent);
    // }

    // [Command]
    // void CmdSendScoreboardUpdated (ActionEvent actionEvent) {
    //     RpcSendScoreboardUpdated (actionEvent);
    // }

    // [ClientRpc]
    // void RpcSendScoreboardUpdated (ActionEvent actionEvent) {
    //     OnScoreboardUpdatedReceived.Invoke (actionEvent);
    // }

    // [TargetRpc]
    // void TargetSendScoreboardUpdated (ActionEvent actionEvent) {
    //     OnScoreboardUpdatedReceived.Invoke (actionEvent);
    // }

}