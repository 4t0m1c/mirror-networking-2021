using System.Collections;
using System.Collections.Generic;
using Mirror;
using UnityEngine;

public class GameManager : Singleton<GameManager> {

    [Header ("Settings")]
    [SerializeField] float warmupTime = 5;
    [SerializeField] float roundTime = 5;
    [SerializeField] float postGameTime = 5;
    [SerializeField] List<Transform> spawnPoints = new List<Transform> ();

    [Header ("Debug")]
    [SerializeField] List<Player> players = new List<Player> ();
    [SerializeField] PickupPoint[] pickupPoints;
    [SerializeField] List<GameObject> pickups = new List<GameObject> ();

    [SerializeField] List<GameObject> pickupPrefabs = new List<GameObject> ();

    bool gameStarted = false;

    public void RegisterPlayer (Player player) {
        players.Add (player);
    }

    public void DeregisterPlayer (Player player) {
        players.Remove (player);
    }

    public void StartGame () {
        if (!gameStarted) {
            gameStarted = true;
            StartCoroutine (GameTimer ());
            SpawnPickups ();
        }
    }

    IEnumerator GameTimer () {
        float currentTime = 0;
        float currentTimeSeconds = 0;
        //Warmup
        WarmupSpawnPoints ();

        while (currentTime < warmupTime) {
            TimeUpdate (ref currentTime, ref currentTimeSeconds);
            yield return null;
        }

        //Game
        for (var i = 0; i < players.Count; i++) {
            players[i].StateGameplay ();
        }

        currentTime = 0;
        currentTimeSeconds = 0;
        while (currentTime < roundTime) {
            TimeUpdate (ref currentTime, ref currentTimeSeconds);
            yield return null;
        }

        //PostGame
        for (var i = 0; i < players.Count; i++) {
            players[i].StatePostGame ();
        }

        currentTime = 0;
        currentTimeSeconds = 0;
        while (currentTime < postGameTime) {
            TimeUpdate (ref currentTime, ref currentTimeSeconds);
            yield return null;
        }

        for (int i = 0; i < pickups.Count; i++) {
            NetworkServer.Destroy (pickups[i]);
        }
        pickups.Clear ();

        //EndGame
        for (var i = 0; i < players.Count; i++) {
            players[i].StateEndGame ();
        }
        while (players.Count > 0) {
            DeregisterPlayer(players[0]);
        }

        MatchMaker.Instance.EndRound ();
        gameStarted = false;
    }

    void WarmupSpawnPoints () {
        List<Transform> _spawnPoints = new List<Transform> ();
        for (int i = 0; i < spawnPoints.Count; i++) {
            _spawnPoints.Add (spawnPoints[i]);
        }
        List<Transform> shuffledSpawnPoints = new List<Transform> ();
        while (_spawnPoints.Count > 0) {
            shuffledSpawnPoints.Add (_spawnPoints[Random.Range (0, _spawnPoints.Count)]);
            _spawnPoints.RemoveAt (0);
        }

        for (var i = 0; i < players.Count; i++) {
            players[i].StateWarmup (shuffledSpawnPoints[0].position);
            shuffledSpawnPoints.RemoveAt (0);
        }
    }

    void TimeUpdate (ref float currentTime, ref float currentTimeSeconds) {
        currentTime += Time.deltaTime;
        if (currentTime - currentTimeSeconds >= 1) {
            currentTimeSeconds = currentTime;

            //Send updated time to players
            for (var i = 0; i < players.Count; i++) {
                players[i].GameTimeUpdated (currentTimeSeconds);
            }
        }
    }

    void SpawnPickups () {
        pickupPoints = GameObject.FindObjectsOfType<PickupPoint> ();

        foreach (var point in pickupPoints) {
            GameObject newPickup = Instantiate (pickupPrefabs[Random.Range (0, pickupPrefabs.Count)]);
            newPickup.transform.position = point.transform.position;

            pickups.Add (newPickup);
            NetworkServer.Spawn (newPickup);
        }
    }

}