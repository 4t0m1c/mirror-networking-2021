using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.InputSystem;

public class InputManager : Singleton<InputManager> {

    public static UnityEvent<Vector2> OnMovementUpdated = new UnityEvent<Vector2> ();
    public static UnityEvent OnInteractPerformed = new UnityEvent ();
    public static UnityEvent OnCancelPerformed = new UnityEvent ();
    public static UnityEvent OnChatPerformed = new UnityEvent ();
    public static UnityEvent OnScoreboardStarted = new UnityEvent ();
    public static UnityEvent OnScoreboardCanceled = new UnityEvent ();

    GameInput gameInput;

    void Awake () {
        gameInput = new GameInput ();
        gameInput.Enable ();

        gameInput.Gameplay.Movement.performed += x => OnMovementUpdated.Invoke (x.ReadValue<Vector2> ());
        gameInput.Gameplay.Interact.performed += x => OnInteractPerformed.Invoke ();

        gameInput.Gameplay.Cancel.performed += x => OnCancelPerformed.Invoke ();
        gameInput.Gameplay.Chat.performed += x => OnChatPerformed.Invoke ();
        
        gameInput.Gameplay.Scoreboard.started += x => OnScoreboardStarted.Invoke ();
        gameInput.Gameplay.Scoreboard.canceled += x => OnScoreboardCanceled.Invoke ();
    }

    void OnDestroy () {
        gameInput.Dispose ();
    }

}