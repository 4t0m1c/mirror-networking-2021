#if UNITY_EDITOR
using UnityEngine;
using System.Diagnostics;
using UnityEditor;
#endif

public class GameBuilder {
#if UNITY_EDITOR

    static bool chainingBuilds = false;
    static string path;

    //Change these scene paths
    static string[] clientScenes = new string[] { "Assets/_PROJECT/Scenes/Offline.unity", "Assets/_PROJECT/Scenes/Online.unity", "Assets/_PROJECT/Scenes/Game.unity" };
    static string[] serverScenes = new string[] { "Assets/_PROJECT/Scenes/Offline.unity", "Assets/_PROJECT/Scenes/Online.unity" };


    [MenuItem ("Build Game/Build for Deployment", false, 1)]
    public static void BuildForDeploy () {
        chainingBuilds = true;
        path = EditorUtility.SaveFolderPanel ("Locate build folder", "../Builds/", "");
        BuildLinuxServer ();
        AssetDatabase.Refresh ();
        BuildWindowsGameClient ();
        AssetDatabase.Refresh ();
        chainingBuilds = false;
    }

    [MenuItem ("Build Game/Build for Local Windows", false, 2)]
    public static void BuildForWindows () {
        chainingBuilds = true;
        path = EditorUtility.SaveFolderPanel ("Locate build folder", "../Builds/", "");
        BuildWinServer (true);
        AssetDatabase.Refresh ();
        BuildWindowsGameClient (true);
        AssetDatabase.Refresh ();
        chainingBuilds = false;
    }

    [MenuItem ("Build Game/Windows Game Client", false, 13)]
    public static void BuildWindowsGameClient () { BuildWindowsGameClient (true); }
    static void BuildWindowsGameClient (bool run = false) {
        // Get filename.
        if (!chainingBuilds) path = EditorUtility.SaveFolderPanel ("Locate build folder", "../Builds/", "");
        string ver = $"/{Application.version}/Client [WINDOWS]/";
        string filename = $"{Application.productName}_{Application.version}[Client].exe";

        // Build player.
        BuildPipeline.BuildPlayer (new BuildPlayerOptions () {
            scenes = clientScenes,
                locationPathName = path + ver + filename,
                target = BuildTarget.StandaloneWindows
        });

        if (run) {
            // Run the game (Process class from System.Diagnostics).
            Process proc = new Process ();
            proc.StartInfo.FileName = path + filename;
            proc.Start ();
        }
    }

    [MenuItem ("Build Game/Linux Server", false, 14)]
    public static void BuildLinuxServer () {
        // Get filename.
        if (!chainingBuilds) path = EditorUtility.SaveFolderPanel ("Locate build folder", "../Builds/", "");
        string ver = $"/{Application.version}/Server [LINUX]/";
        string filename = $"{Application.productName}_{Application.version}[SERVER].x86_64";

        // Build player.
        BuildPipeline.BuildPlayer (new BuildPlayerOptions () {
            scenes = serverScenes,
                locationPathName = path + ver + filename,
                target = BuildTarget.StandaloneLinux64,
                options = BuildOptions.EnableHeadlessMode
        });
    }

    [MenuItem ("Build Game/Windows  Server", false, 34)]
    public static void BuildWinServer () { BuildWinServer (true); }
    static void BuildWinServer (bool run = false) {
        // Get filename.
        if (!chainingBuilds) path = EditorUtility.SaveFolderPanel ("Locate build folder", "../Builds/", "");
        string ver = $"/{Application.version}/Server [WINDOWS]/";
        string filename = $"{Application.productName}_{Application.version}[SERVER].exe";

        // Build player.
        BuildPipeline.BuildPlayer (new BuildPlayerOptions () {
            scenes = serverScenes,
                locationPathName = path + ver + filename,
                target = BuildTarget.StandaloneWindows64,
                options = BuildOptions.EnableHeadlessMode
        });

        if (run) {
            // PROCESS SPAWNER
            Process proc = new Process ();
            proc.StartInfo.FileName = path + ver + filename;
            proc.StartInfo.Arguments = "-s";
            proc.Start ();
        }
    }

#endif 
}