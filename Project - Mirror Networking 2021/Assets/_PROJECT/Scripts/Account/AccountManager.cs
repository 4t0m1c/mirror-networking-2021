using System.Collections;
using System.Collections.Generic;
using PlayFab;
using PlayFab.ClientModels;
using UnityEngine;
using UnityEngine.Events;

public class AccountManager : Singleton<AccountManager> {

    public static UnityEvent OnAccountSuccess = new UnityEvent ();
    public static UnityEvent OnPlayerProfileReceived = new UnityEvent ();
    public static PlayerProfileModel playerProfileModel;

    public void CreateAccount (string username, string email, string password, UnityAction<string> errorAction) {
        PlayFabClientAPI.RegisterPlayFabUser (new RegisterPlayFabUserRequest () {
                Email = email,
                    Username = username,
                    Password = password
            },
            result => {
                Debug.Log ($"Successful Account Creation");

                PlayFabClientAPI.UpdateUserTitleDisplayName (new UpdateUserTitleDisplayNameRequest () {
                    DisplayName = username
                }, result => {
                    SignIn (username, password, null);
                }, error => {
                    Debug.Log (error.ErrorMessage);
                });

            },
            error => {
                errorAction.Invoke (error.ErrorMessage);
                Debug.Log (error.ErrorMessage);
            }
        );
    }

    public void SignIn (string username, string password, UnityAction<string> errorAction) {
        PlayFabClientAPI.LoginWithPlayFab (new LoginWithPlayFabRequest () {
                Username = username,
                    Password = password
            },
            result => {
                Debug.Log ($"Successful Login");

                PlayFabClientAPI.GetPlayerProfile (new GetPlayerProfileRequest (),
                    result => {
                        playerProfileModel = result.PlayerProfile;
                        Debug.Log ($"Retrieved Player Profile: {playerProfileModel.DisplayName}");
                        OnPlayerProfileReceived.Invoke ();
                    }, error => {
                        Debug.Log (error.ErrorMessage);
                    });

                OnAccountSuccess.Invoke ();
            },
            error => {
                errorAction.Invoke (error.ErrorMessage);
                Debug.Log (error.ErrorMessage);
            });
    }

}