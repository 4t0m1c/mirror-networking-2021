using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIAccountInput : MonoBehaviour {

    [SerializeField] Text createAccountError, signInError;

    string username, email, password;

    void OnEnable () {
        AccountManager.OnAccountSuccess.AddListener (DisableUI);
    }

    void OnDisable () {
        AccountManager.OnAccountSuccess.RemoveListener (DisableUI);
    }

    void DisableUI () {
        gameObject.SetActive (false);
    }

    public void CreateAccount () {
        Debug.Log ($"UI Create Account");
        AccountManager.Instance.CreateAccount (username, email, password, DisplayErrorCreateAccount);
    }

    public void SignIn () {
        Debug.Log ($"UI Sign In");
        AccountManager.Instance.SignIn (username, password, DisplayErrorSignIn);
    }

    public void InputUsername (string _username) {
        username = _username;
    }

    public void InputEmail (string _email) {
        email = _email;
    }

    public void InputPassword (string _password) {
        password = _password;
    }

    void DisplayErrorCreateAccount (string error) {
        createAccountError.gameObject.SetActive (true);
        createAccountError.text = error;
    }

    void DisplayErrorSignIn (string error) {
        signInError.gameObject.SetActive (true);
        signInError.text = error;
    }

}